#include <AccelStepper.h>
#include <FastLED.h>

// Motor
#define MOTOR_INTERFACE_TYPE 4
#define BASE_ROTATION 3000

// LED stuff
#define NUM_LEDS 17
#define DATA_PIN 3

AccelStepper myStepper(MOTOR_INTERFACE_TYPE, 8, 10, 9, 11);
CRGB leds[NUM_LEDS];
void updateLEDs() {
  int ledIndex = map(abs(myStepper.currentPosition()), 0, BASE_ROTATION, 0, NUM_LEDS);

  fill_solid(leds, NUM_LEDS, CRGB::Black);
  leds[ledIndex] = CRGB::Red;

  FastLED.show();
}
void setup() {
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

	myStepper.setMaxSpeed(1000.0);
	myStepper.setAcceleration(200.0);
	myStepper.setSpeed(200);
	myStepper.moveTo(BASE_ROTATION);
  
  fill_solid(leds, NUM_LEDS, CRGB::Black);
  FastLED.show();
}

void loop() {
	if (myStepper.distanceToGo() == 0) {
    delay(5000);
    myStepper.moveTo(-myStepper.currentPosition());
  }
	myStepper.run();

  updateLEDs();
}

